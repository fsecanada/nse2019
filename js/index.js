$(function(){
	//=========== when click login button ==================
	$('#bt_login').click(function() {
		if($('#user').val() == "admin" && $('#pass').val() == "admin") {
			location.href = "home.html";
			localStorage.setItem('user', 'admin');
		} else {
			alert("login error.");
		}
	})
	//=========== when press enter key in password input ==================
	$('#pass').keypress(function(e) {
	    if(e.which == 13) {
	        $('#bt_login').click();
	    }
	});

//============= main page ================
if($('.main-wrapper').length > 0) {

	var ctx = null;

	//============= show user name and current date on top banner ================
	user = localStorage.getItem('user');
	date = new Date();
	ary = date.toString().split(' ');
	str = "WELCOME " + user + "," + ary[0] + " " + ary[1] + " " + ary[2] + " " + ary[3] + " " + ary[4];
	$('#header #username').html(str);
	$('#mobile_header #username').html(user);

	//============ if not login, redirect to login page =================
	if(user != "admin") {
		location.href = "index.html";
	}

	//============= the code to be responsive, fire when resize the browser or mobile size  ================
	resize();
	$(window).resize(function(){
		resize();
	})
	function resize() {
		ww = $('.wrapper').width();
		hh = $(window).height();
		ww = ww - 270;
		hh = hh/2 - 80;
		$('.mid').css('width', ww+'px');
		$('.div_empty').css('top', hh+'px');

		hh = $(window).height();
		hh = hh - 450;
		ww = $('.div_chart').width();

		//$('.chart').attr('style', 'width: '+ww+'px !important;height: '+hh+'px !important');

		//$('.chart').attr('width', ww);
		//$('.chart').attr('height', hh);
	}

	//================= initialize date input =======================

	$( "#sfilter #date_from1" ).datepicker();
	$( "#sfilter #date_to1" ).datepicker();
	$( "#header #date_from" ).datepicker();
	$( "#header #date_to" ).datepicker();

	//============ logout function =================
	$('#bt_logout').click(function() {
		localStorage.setItem('user', '');
		location.href = "index.html";
	})

	//============= when click main menu item (not submenu item) ================
	$('.mitem').click(function() {
		obj = $(this).next(); //============= get current menu object clicked ================
		//============ hide submenu for other menu and show submenu for current menu =================
		$('.submenu').removeClass('openmenu');
		$('.mitem').find('.fa').addClass('fa-chevron-down');
		$('.mitem').find('.fa').removeClass('fa-chevron-up');

		obj.toggleClass('openmenu');
		if(obj.hasClass('openmenu')) {
			$(this).find('.fa').removeClass('fa-chevron-down');
			$(this).find('.fa').addClass('fa-chevron-up');
		} else {
			$(this).find('.fa').addClass('fa-chevron-down');
			$(this).find('.fa').removeClass('fa-chevron-up');
		}
		
		//============== calculate sumenu's count and display main boxs in main body===============
		maincate = $(this).find('span').html();
		$('.main_title').html(maincate);
		
		if(isMobile()) $('.left_menu').fadeOut('fast');


		$('#main_item_wrapper').html("");
		$('#sitem_wrapper').html("");
		n = 1;
		cn = obj.find('.menu_item').length;
		if(cn*1 > 3) $('.marrow').fadeIn();
		else $('.marrow').fadeOut();

		sitem_sw = 70;
		if(isMobile()) sitem_sw = 140;
		$('#main_item_wrapper').css('width', cn*main_sw+'px');
		$('#main_item_slider').css('width', 3*main_sw+'px');
		$('#sitem_wrapper').css('width', cn*sitem_sw+'px');
		$('#main_item_wrapper').css('margin-left', '0px');
		if(cn*1 < 3) {
			$('#main_item_wrapper').css('margin', 'auto');
		}

		obj.find('.menu_item').each(function(){
			txt = $(this).text();
			if($(this).find('span').length > 0) {
				txt = $(this).find('span').text();
			}
			if($(this).find('select').length > 0) {
				txt += $(this).find('select').val();
			}
			strHtml = `<div class='main_item' id='item`+n+`'>
                                    <div class='item_top'>`+txt+`</div>
                                    <div class='item_title'></div>
                                </div>`;
			$('#main_item_wrapper').append(strHtml);
			strHtml = `<div class='sitem' id='sitem`+n+`'>
                                    <div class="mchart_slider_item">
                                        <div class='item_top'>`+txt+`</div>
                                        <div class='item_title'></div>
                                    </div>
                                </div>`;
			$('#sitem_wrapper').append(strHtml);
			loaddata(txt, n);
			n ++;
		});

		//============= main box click event ================
		mainClickEvent(); 
		
		//============= click the first box by default after show main boxs ================
		$('#item1').click();
	})
	
	//============= click event for submenu item , similar the login to mainmenu click ================
	$('.submenu .menu_item').click(function(ev) { 
		tObj = $(ev.target);
		if(tObj.hasClass('menu_item')) {}
		else {
			return;
		}
		$('.submenu .menu_item').removeClass('menu_item_sel');
		$(this).addClass('menu_item_sel');
		obj = $(this);
		idx = $(this).attr('id');
		sel_menu = idx;
		
		maincate = "";
		maintitle = $(this).text();
		if($(this).find('span').length > 0) {
			maintitle = $(this).find('span').text();
		}
		if($(this).find('select').length > 0) {
			maintitle += $(this).find('select').val();
		}

		$('#item1 .item_top').html(maintitle);
		$('#sitem1 .item_top').html(maintitle);
		
		$('.main_title').html(maintitle);

		if(isMobile()) $('.left_menu').fadeOut('fast');


		if(!checkSelClient()) {
			$('.mid').fadeOut(); 
			$('.div_empty').fadeIn(); 
			return;
		}

		$('#main_item_wrapper').html("");
		$('#sitem_wrapper').html("");
		n = 1;
		$('.marrow').fadeOut();
		$('.sprev').fadeOut();
		$('.snext').fadeOut();
		$('#main_item_wrapper').css('width', main_sw+'px');
		$('#main_item_slider').css('width', main_sw+'px');
		$('#main_item_wrapper').css('margin-left', 'auto');
		$('#main_item_wrapper').css('margin', 'auto');

		txt = maintitle;
		strHtml = `<div class='main_item main_item_sel' id='item`+n+`'>
                                <div class='item_top'>`+txt+`</div>
                                <div class='item_title'></div>
                            </div>`;
		$('#main_item_wrapper').append(strHtml);
		strHtml = `<div class='sitem sitem_sel' id='sitem`+n+`' style='display:none'>
                                <div class="mchart_slider_item">
                                    <div class='item_top'>`+txt+`</div>
                                    <div class='item_title'></div>
                                </div>
                            </div>`;
		$('#sitem_wrapper').append(strHtml);
		mainClickEvent();

		$('#bt_show').click();
	})
	$('.submenu .menu_item select').change(function(){
		$(this).parent().click();
	})


	var main_sw = 210; //============= default size for main box ================
	var sitem_sw = 140; //============ default size of box in below chart =================
	var bClick = true;

	//============= event when click main box (above chart) ================
	function mainClickEvent() {  
		//============= event when click main box ================
		$('.main_item').click(function() {
			
			$('.main_item').removeClass('main_item_sel');
			obj = $(this);
			obj.addClass('main_item_sel');
			idx = $(this).attr('id').replace('item', '');
			
			maintitle = $(this).find('.item_top').html(); 
			
			$('.sitem').removeClass('sitem_sel');
			$('#sitem'+idx).addClass('sitem_sel');

			$('#bt_show').click();
		});
		//============== event when click the right arrow in main box ===============
		$('.mnext').click(function (){ console.log(bClick);
			if(!bClick) return;
			bClick = false;
			setTimeout(function(){bClick=true;}, 800);
			id = $('#main_item_wrapper').css('margin-left').replace('px', '');
			id = Math.abs(Math.round(id/main_sw)); 
			cn = $('#main_item_wrapper .main_item').length;
			idx = $('.main_item_sel').attr('id').replace('item', '');
			if(id*1 < (cn-3)) { showLoading(); setTimeout(function(){stopLoading();}, 800);
				id = id*1 + 1;
				mgl = -(id)*main_sw; 
				$('#main_item_wrapper').animate({'margin-left': mgl+'px'}, 300, function(){
					//$('#item'+(id*1+1)).click();
					
				});
			}
			idx = $('.main_item_sel').attr('id').replace('item', '');
			if(idx*1 < cn) {
				idx = idx*1 + 1;
				$('#item'+idx).click();
			}

		})
		//============event when click left arrow in main box =================
		$('.mprev').click(function (){ 
			if(!bClick) return;
			bClick = false;
			setTimeout(function(){bClick=true;}, 800);
			id = $('#main_item_wrapper').css('margin-left').replace('px', '');
			id = Math.abs(Math.round(id/main_sw)); 
			if(id*1 > 0) { showLoading(); setTimeout(function(){stopLoading();}, 800);
				id = id*1 - 1;
				mgl = -(id)*main_sw; 
				$('#main_item_wrapper').animate({'margin-left': mgl+'px'}, 300, function(){
					//$('#item'+(id*1+1)).click();
					
				});
			}
			idx = $('.main_item_sel').attr('id').replace('item', '');
			if(idx*1 > 0) {
				idx = idx*1 - 1;
				$('#item'+idx).click();
			}
		})

		//============= event when click chart slide box in below chart ================
		$('.sitem').click(function() {
			$('.sitem').removeClass('sitem_sel');
			obj = $(this);
			obj.addClass('sitem_sel');
			idx = $(this).attr('id').replace('sitem', '');
			
			maintitle = $('#item'+idx).find('.item_top').html(); 
			
			loadChart(maintitle);
			
			cn = $('#main_item_wrapper .main_item').length;
			if(cn*1 > 3) {
				mgl = -(idx-1)*main_sw;
				if(idx > (cn-3)) mgl = -(cn-3)*main_sw;
				$('#main_item_wrapper').animate({'margin-left': mgl+'px'}, 300);
			}
			$('.main_item').removeClass('main_item_sel');
			$('#item'+idx).addClass('main_item_sel');
		});
		//============== event when click right arrow in below chart ===============
		$('.snext').click(function(e) {
			id = $('.sitem_sel').attr('id').replace('sitem', '');
			cn = $('#main_item_wrapper .main_item').length;
			if(id*1 < cn) {
				id = id*1 + 1;
				mgl = -(id-1)*sitem_sw;
				$('.slider_wrapper').animate({'margin-left': mgl+'px'}, 300);
				$('#sitem'+id).click();
			}
		});
		//============= event when click left arrow in below chart ================
		$('.sprev').click(function() {
			id = $('.sitem_sel').attr('id').replace('sitem', '');
			if(id*1 > 1) {
				id = id - 1;
				mgl = -(id-1)*sitem_sw;
				$('.slider_wrapper').animate({'margin-left': mgl+'px'}, 300);
				$('#sitem'+id).click();
			}
		});
		//============= event when swipe on mobile device ================
		$(".div_chart").swipe( {
	        swipe:function(event, direction, distance, duration, fingerCount, fingerData) {
	        	if(direction == "left") {
	        		$('.snext').click();
	        	} else if(direction == "right") {
	        		$('.sprev').click();
	        	}
	        },
	        threshold:0
        });
	}

	//============= display data when select time interval on desktop version ================
	$('#day_interval').change(function(){
		$('#bt_show').click();
	})
	//============= display data when select time interval on mobile version ================
	$('#sel_interval').change(function(){
		$('#bt_show').click();
	})
	//============= display data when click show button on mobile version ================
	$('#bt_show1').click(function() {
		$('#bt_show').click();
	})
	//============= display data when click show button on desktop version ================
	$('#bt_show').click(function() {
		
		date_from = $('#header #date_from').val();
		date_to = $('#header #date_to').val();
		if(isMobile()) {
			date_from = $('#sfilter #date_from1').val();
			date_to = $('#sfilter #date_to1').val();
		}
		interval = $('#day_interval').val();
		if(isMobile()) interval = $('#sel_interval').val();

		if(date_from == "") date_from = "1/1/2016";
		if(date_to == "") date_to = "12/31/2017";

		if(!checkSelClient()) {
			$('.mid').fadeOut(); 
			$('.div_empty').fadeIn(); 
			return;
		}
		$('.div_empty').fadeOut(); 
		$('.mid').fadeIn();
		
		$('.main_item').each(function(){
			txt = $(this).find('.item_top').text();
			idx = $(this).attr('id').replace('item', ''); 
			loaddata(txt, idx);  //=========== display data on main box ================
		})
		loadChart(maintitle); //=========== display chart =============
	})

	//============= display data when select client on desktop version ================
	$('#header #sel_client').change(function(){
		val = $(this).val();
		$('.mid').fadeOut(function(){
			initBoxTitle();
			loadjson(val, true);
			//$('#bt_show').click();
		});
	})
	//============= display data when select client on mobile version ================
	$('#mobile_header #sel_client').change(function(){
		val = $(this).val();
		$('.mid').fadeOut(function(){
			initBoxTitle();
			loadjson(val, true);
			//$('#bt_show').click();
		});	
	});
	//============= display data when select client ID on desktop version ================
	$('#header #sel_id').change(function(){
		sel_id = $(this).val();
		$('#bt_show').click();
	})
	//============= display data when select client ID  on mobile version ================
	$('#mobile_header #sel_id').change(function(){
		sel_id = $(this).val();
		$('#bt_show').click();
	})

	function initBoxTitle(){
		//$('#item1 .item_top').html('TotalUnit');
		//$('#sitem1 .item_top').html('TotalUnit');
	}

	var maincate = "";
	var maintitle = "";

	//============== export PDF ================
	form = $('.mid');
	cache_width = form.width();
	a4 = [595.28, 841.89];

	$('#bt_export').click(function (){
		$('body').scrollTop(0);
  		createPDF();
	})
	$('#bt_export1').click(function (){
		$('#bt_export').click();
	})
	//create pdf
	function createPDF() {
		getCanvas().then(function(canvas) {
			var img = canvas.toDataURL("image/png");
			var doc = new jsPDF({
			 	unit: 'px',
			 	format: 'a4'
			});
			doc.addImage(img, 'JPEG', 20, 20);
			doc.save('Client'+val+'.pdf');
			form.width(cache_width);
		});
	}
	// create canvas object
	function getCanvas() {
		form.width((a4[0] * 1.33333) - 40).css('max-width', 'none');
		return html2canvas(form, {
			imageTimeout: 2000,
			removeContainer: true
		});
	}

	//============== check to select client ================
	function checkSelClient() {
		val = $('#header #sel_client').val();
		if(isMobile()) val = $('#mobile_header #sel_client').val();
		if(val*1 > 0) return true;
		return false;
	}
	//============= show/hide menu when click top menu button on mobile version ================
	$('#btn_menu').click(function(){ 
		$('.left_menu').toggle();
	})

	//============= initialize variables ================
	var jsondata = [];
	var ids = [];
	var sel_id = 0;
	var date_from = "";
	var date_to = "";
	var interval = 0;
	var sel_menu = "";
	var oneDay  = 24*60*60*1000;

	//================== convert date to 'YYYY-MM-DD' =====================
	function convDate(de) {
		var months = ['', 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
		ary = de.split('-');
		yy = ary[2];
		mm = ary[1];
		dd = ary[0]; 
		mm = months.indexOf(mm); 
		if(mm*1 < 10) mm = '0'+mm*1;
		if(dd*1 < 10) dd = '0'+dd*1;
		yy = yy*1 + 2000;
		de = yy+'-'+mm+'-'+dd;
		return de;
	}
	//============= format key, date in data(object) from csv file ================
	//============= remove space from key ================
	function formatJson() {
		cid = "";
		for(i=0; i<jsondata.length-1; i++) {
			for(j=i+1; j<jsondata.length; j++) {
				if(Date.parse(jsondata[i]['RecordDate']) > Date.parse(jsondata[j]['RecordDate'])) {
					temp = jsondata[i];
					jsondata[i] = jsondata[j];
					jsondata[j] = temp;
				}
			}
		}
		ret = [];
		for(i=0; i<jsondata.length; i++) {
			jsondata[i]['Date'] = jsondata[i]['RecordDate'];
			rec = jsondata[i];
			obj = [];
			obj['Date'] = convDate(jsondata[i]['Date']); 
			$.each(rec, function(key, value) { 
				kk = getKeyConv(key);
	            obj[kk] = value.toString().replace(',', '');
	        });
	        if(ids.indexOf(rec['Farm ID']) < 0)
	        	ids.push(rec['Farm ID']);
	        ret.push(obj);
		}
		jsondata = ret;
	}
	//============= funtion to remove space from string ================
	function getKeyConv(str) {
		str = str.toLowerCase();
		str = str.replace(/\s/g, '');
		//str = str.replace('%', '');
		return str;
	}
	//============= load data from csv file and put it to jsondata variable ================
	function loadjson(idx, binit=false) {
		var csvfile = "DashboardData";
		if(idx*1 > 0) csvfile = csvfile + "-Client6";
		$.ajax({
		    url: "data/"+csvfile+".csv",
		    async: false,
		    success: function (csvd) { 
		        var items = $.csv.toObjects(csvd); 
		        var jsonobject = JSON.stringify(items);
		        jsondata = items; 
		        formatJson();
		        
		        if(binit) {
		        	$('#sel_id').html('<option value="0">All</option>');
		        	for(i=0;i<ids.length;i++) {
		        		$('#header #sel_id').append('<option value="'+ids[i]+'">'+ids[i]+'</option>');
		        		$('#mobile_header #sel_id').append('<option value="'+ids[i]+'">'+ids[i]+'</option>');
		        	}
		        	sel_id = $('#sel_id').val();

		        	minDate = jsondata[0].Date;
					maxDate = jsondata[jsondata.length-1].Date;
					//minDate = getPrevMonthDate(maxDate);

					$( "#header #date_from" ).datepicker("option", "minDate", new Date(minDate) );
					$( "#header #date_from" ).datepicker("option", "maxDate", new Date(maxDate) );
					$( "#header #date_to" ).datepicker("option", "minDate", new Date(minDate) );
					$( "#header #date_to" ).datepicker("option", "maxDate", new Date(maxDate) );
					$( "#header #date_from" ).datepicker("setDate", new Date(getPrevMonthDate(maxDate)) );
					$( "#header #date_to" ).datepicker("setDate", new Date(maxDate) );

					$( "#sfilter #date_from1" ).datepicker("option", "minDate", new Date(minDate) );
					$( "#sfilter #date_from1" ).datepicker("option", "maxDate", new Date(maxDate) );
					$( "#sfilter #date_to1" ).datepicker("option", "minDate", new Date(minDate) );
					$( "#sfilter #date_to1" ).datepicker("option", "maxDate", new Date(maxDate) );
					$( "#sfilter #date_from1" ).datepicker("setDate", new Date(getPrevMonthDate(maxDate)) );
					$( "#sfilter #date_to1" ).datepicker("setDate", new Date(maxDate) );

					$('#bt_show').click();
					setTimeout(function() {
						$('#mitem1').click();	
					}, 200);
		        }
		    },
		    dataType: "text",
		    complete: function () {
		        // call a function on complete 
		    }
		});
	}


	//============= display data in main box from jsondata variable ================
	function loaddata(maintitle="", item_idx=1) { // maintile : menu name to show in main box when click left menu,  item_idx : menu item's id
		sum1 = 0;
		sum2 = 0;
		sum3 = 0;
		nn = 1;
		if(maintitle == "") return;

		for(i=0; i<jsondata.length; i++) {
			rec = jsondata[i];
			date = rec.Date; 
			id = rec['farmid'];

			val = $('#header #sel_client').val(); 
			if(isMobile()) val = $('#mobile_header #sel_client').val();
			
			if((sel_id == 0) || (sel_id != 0 && sel_id == id)) {
				if(formatDate(date) >= formatDate(date_from) && formatDate(date) <= formatDate(date_to)) {
					mt = getKeyConv(maintitle);
					if(val*1 > 0) { 
						sum1 = sum1 *1 + rec[mt]*1;
					}		
					nn ++;
				}
			}
		}
		
		if(0) {
			sum1 = sum1 / nn;
		}

		$('#item'+item_idx+' .item_title').html(sum1.toFixed(2)); 
		$('#sitem'+item_idx+' .item_title').html(sum1.toFixed(2));
	}


	//============= diaply chart from jsondata variable ================
	var myChart = null;
	
	var opt = {
	        title: {
	            display: true,
	            text: ''
	        },
	        legend: {
	            display: false,
	            labels: {
	                fontColor: '#f00'
	            }
	        },
	        responsive: true,
	        maintainAspectRatio: false
	      };

	//============= function to diaply chart from jsondata variable ================
	function loadChart(maintitle="") { // maintile : menu name to show in main box when click left menu
		if(myChart != null) myChart.destroy();
		if(maintitle == "") return;

		showLoading();
		lbdata = [];
		data1 = [];

		//============= display chart when time interval is 1 day , it is default ================
		if(interval*1 == 1) { 
			for(i=0; i<jsondata.length; i++) {
				date = jsondata[i].Date; 
				id = jsondata[i]['farmid'];
				if((sel_id == 0) || (sel_id != 0 && sel_id == id)) {
					if(formatDate(date) >= formatDate(date_from) && formatDate(date) <= formatDate(date_to)) {
						lbdata.push(date); 
						da = "";
						mt = getKeyConv(maintitle);
						da = jsondata[i][mt];
						data1.push(da);
					}
				}
			}
		} else {
		//============= when time interver is not 1 day, ================
			//============= gary varialble : have date as key, initialize it's values to 0.0 ================
			dd = "";
			da = 0;
			gary = {};
			for(i=0; i<jsondata.length; i++) {
				date = jsondata[i].Date; 
				id = jsondata[i]['farmid'];
				if((sel_id == 0) || (sel_id != 0 && sel_id == id)) {
					if(formatDate(date) >= formatDate(date_from) && formatDate(date) <= formatDate(date_to)) {
						gdate = getMonth(date);
						if(interval == "90")
							gdate = getQuater(date);
						else if(interval == "180")
							gdate = getSeme(date);
						if(interval == "360")
							gdate = getYear(date);
						gary[gdate] = 0.0;
					}
				}
			}
			//============= calculator the value of gary variable, sum of value for time interval  ================
			for(i=0; i<jsondata.length; i++) {
				date = jsondata[i].Date; 
				id = jsondata[i]['farmid'];
				if((sel_id == 0) || (sel_id != 0 && sel_id == id)) {
					if(formatDate(date) >= formatDate(date_from) && formatDate(date) <= formatDate(date_to)) {						
						gdate = getMonth(date);
						if(interval == "90")
							gdate = getQuater(date);
						else if(interval == "180")
							gdate = getSeme(date);
						if(interval == "360")
							gdate = getYear(date);

						mt = getKeyConv(maintitle);
						gary[gdate] = gary[gdate]*1 + jsondata[i][mt] *1;
					}
				}
			}
			//============= get label and value to show on chart from gary variable ================
			$.each(gary, function(key, value) { 
				lbdata.push(key); 
				data1.push(value);
	        });
		}
		stopLoading();

		//============= display chart with lbdata and data1 variable ================
		chartType = 'bar';
		if(0) {
			chartType = 'line';
		}
		ctx = document.getElementById("chart").getContext('2d');
		myChart = new Chart(ctx, {
		  type: chartType,
		  data: {
		    labels: lbdata,
		    datasets: [{
		      label: 'TotalUnit',
		      data: data1,
		      backgroundColor: "#1f77b4"
		    }]
		  },
		  options: opt
		});
	}

	//============= get "YYYY-MM" from date ================
	function getMonth(de) {
		ary = de.split('-');
		yy = ary[0];
		mm = ary[1];
		return yy+'-'+mm;
	}
	//============= get "YYYY-Q" from date : get quater from the date ================
	function getQuater(de) {
		ary = de.split('-');
		yy = ary[0];
		mm = ary[1];
		if(mm*1 <= 3) mm = 1;
		else if(mm*1 > 3 && mm*1<=6) mm =2;
		else if(mm*1 > 6 && mm*1<=9) mm =3;
		else if(mm*1 > 9) mm = 4;
		return yy+'-'+mm;
	}
	//============= get "YYYY-S" from date : get seme from the date ================
	function getSeme(de) {
		ary = de.split('-');
		yy = ary[0];
		mm = ary[1];
		if(mm*1 <= 6) mm = 1;
		else if(mm*1 > 6) mm = 2;
		return yy+'-'+mm;
	}
	//============= get "YYYY" from date : get year from the date ================
	function getYear(de) {
		ary = de.split('-');
		yy = ary[0];
		return yy;
	}
	//============= get date 1 month ago from current date ================
	function getPrevMonthDate(de) {
		ary = de.split('-');
		yy = ary[0];
		mm = ary[1];
		dd = ary[2];
		if(mm*1 == 1) {
			yy = yy*1 - 1;
			mm = 12;
		} else {
			mm = mm*1 - 1;
		}
		return yy+'-'+mm+'-'+dd;
	}
	//============= get timestamp from date for compare date ================
	function formatDate(de) { return Date.parse(de);
		ary = de.split('/');
		yy = ary[2];
		mm = ary[0];
		dd = ary[1];
		if(mm*1 < 10) mm = '0'+mm;
		if(dd*1 < 10) dd = '0'+dd;
		de = yy+'-'+mm+'-'+dd;
		return Date.parse(de);
	}
	//============= get next date (YYYY-MM) from YYYY-MM ================
	function nextMonth(yyyymm, stm) {
		return eval(yyyymm*1+stm*1);
	}
	function getDate(dd) {
		ary = de.split('/');
		yy = ary[2];
		mm = ary[0];
		dd = ary[1];
	}
	//------------------------------
	
}
//============= detect mobile device ================
function isMobile() {
	if($(window).width() < 1024) return true;
	return false;
}
//============= show loading spinner ================
function showLoading() {
	$('#loading').css('display', 'block');
}
//============= hide loading spinner ================
function stopLoading() {
	$('#loading').css('display', 'none');
}

})